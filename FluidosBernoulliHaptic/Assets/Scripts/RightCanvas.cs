﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class RightCanvas : MonoBehaviour {

	// Setters

	// Object size
	public void setSliderEdge(string newText)
	{
		float temp = float.Parse (newText);
		temp = (temp > ObjectBehaviour.instance.edge.maxValue) ? ObjectBehaviour.instance.edge.maxValue : temp;
		temp = (temp < ObjectBehaviour.instance.edge.minValue) ? ObjectBehaviour.instance.edge.minValue : temp;

		ObjectBehaviour.instance.edge.value = temp;
		temp = temp/10.0f; // para pasarlo de cm a escala
		ObjectBehaviour.instance.transform.localScale = new Vector3 (temp, temp, temp);
	}

	public void setTextEdge(float newValue)
	{
		ObjectBehaviour.instance.edgeIF.text = newValue.ToString();
		float temp = newValue / 10.0f;
		ObjectBehaviour.instance.transform.localScale = new Vector3 (temp, temp, temp);
	}


	// Object density
	public void setSlidertDensityObject(string newText)
	{
		float temp = float.Parse (newText);
		temp = (temp > ObjectBehaviour.instance.densityObject.maxValue) ? ObjectBehaviour.instance.densityObject.maxValue : temp;
		temp = (temp < ObjectBehaviour.instance.densityObject.minValue) ? ObjectBehaviour.instance.densityObject.minValue : temp;

		ObjectBehaviour.instance.densityObject.value = temp;
	}

	public void setTextDensityObject(float newValue)
	{
		ObjectBehaviour.instance.densityObjectIF.text = newValue.ToString();
	}

	public void currentSelectionObjectToggle(bool change)
	{
		Toggle temp = ObjectBehaviour.instance.densityObjectTG.ActiveToggles ().FirstOrDefault ();


		Debug.Log (temp.name);

		float value = 0.0f;

		if (temp.name == "DensityCopper") {
			ObjectBehaviour.instance.GetComponent<Renderer>().material = 
				ObjectBehaviour.instance.possibleMaterials [0];
			value = 8920.0f;
		} else if (temp.name == "DensityWood") {
			ObjectBehaviour.instance.GetComponent<Renderer>().material = 
				ObjectBehaviour.instance.possibleMaterials [1];
			value = 500.0f;
		} else if (temp.name == "DensityPET") {
			ObjectBehaviour.instance.GetComponent<Renderer>().material = 
				ObjectBehaviour.instance.possibleMaterials [2];
			value = 1400.0f;
		} else if (temp.name == "DensityIron") {
			ObjectBehaviour.instance.GetComponent<Renderer>().material = 
				ObjectBehaviour.instance.possibleMaterials [3];
			value = 7860.0f;
		} else {
			value = 500.0f;
		}
		ObjectBehaviour.instance.densityObject.value = value;
		ObjectBehaviour.instance.densityObjectIF.text = value.ToString();

	}

	// Liquid density
	public void setSliderDensityLiquid(string newText)
	{
		float temp = float.Parse (newText);
		temp = (temp > ObjectBehaviour.instance.densityLiquid.maxValue) ? ObjectBehaviour.instance.densityLiquid.maxValue : temp;
		temp = (temp < ObjectBehaviour.instance.densityLiquid.minValue) ? ObjectBehaviour.instance.densityLiquid.minValue : temp;
		ObjectBehaviour.instance.densityLiquid.value = temp;
		changeTransparency (temp);
		//ObjectBehaviour.Update ();
	}

	public void setTextDensityLiquid(float newValue)
	{
		// Set the input field value
		ObjectBehaviour.instance.densityLiquidIF.text = newValue.ToString();
		changeTransparency (newValue);
	}
		
	public void currentSelectionLiquidToggle(bool change)
	{
		Toggle temp = LiquidBehavior.instance.densityLiquidTG.ActiveToggles ().FirstOrDefault ();
		Renderer rend = LiquidBehavior.instance.GetComponent<Renderer> ();

		Color newColor = Color.cyan;
		float value = 0.0f;

		if (temp.name == "DensityWater") {
			value = 1000.0f;
		} else if (temp.name == "DensityOil") {
			newColor = Color.yellow;
			value = 920.0f;
		} else if (temp.name == "DensityMercury") {
			newColor = Color.gray;
			value = 13600.0f;
		} else if (temp.name == "DensityAlcohol") {
			newColor = Color.white;
			value = 806.0f;
		} else {
			value = 806.0f;
		}

		rend.material.color = newColor;


		ObjectBehaviour.instance.densityLiquid.value = value;
		setTextDensityLiquid (value);

	}

	public void changeTransparency(float newValue)
	{
		// Change the liquid transparency
		Renderer rend = LiquidBehavior.instance.GetComponent<Renderer> ();

		Color currentColor = rend.material.color;

		float alphaVal = (newValue - ObjectBehaviour.instance.densityLiquid.minValue) /
			(ObjectBehaviour.instance.densityLiquid.maxValue - ObjectBehaviour.instance.densityLiquid.minValue);

		//Adjust Alpha value to never be greater than half value and add a shift
		currentColor.a = ( alphaVal* 0.5f ) + 0.25f;  // New range (0.25,0.75)
		rend.material.color = currentColor;
	}

}
