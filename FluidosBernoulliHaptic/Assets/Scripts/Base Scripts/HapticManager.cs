﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class HapticManager : MonoBehaviour 
{
	// plugin import
	private IntPtr myHapticPlugin;
	// haptic thread
	private Thread myHapticThread;
	// a flag to indicate if the haptic simulation currently running
	private bool hapticThreadIsRunning;
	// haptic devices in the scene
	// haptic workspace
	float workspace = 25.0f;
	// number of haptic devices detected
	private int hapticDevices;
	private HapticInteractionPoint[] hips;
	public GameObject[] ihips;

	// I will use the first haptic device 
	// int i = 0;

	// Left haptic device
	private Vector3 forceField;
	private float maxForceField = 10.0f;

	public static Vector3 setNormalPositiveAndOne(Vector3 normal)
	{
		Vector3 temp = new Vector3 (0, 0, 0);
		temp.x = (normal.x != 0) ? Mathf.Abs(normal.x ) : 0;
		temp.y = (normal.y != 0) ? Mathf.Abs(normal.y ) : 0;
		temp.z = (normal.z != 0) ? Mathf.Abs(normal.z ) : 0;

		return temp;
	}

	private void SetForceByDesiredPosition(int hapDevNum)
	{
		// compute linear force    

		Vector3 tempNormal = setNormalPositiveAndOne(hips[hapDevNum].contactNormal);

		if (hips [hapDevNum].button0) {
			hips [hapDevNum].error0 = hips [hapDevNum].desiredPosition - hips [hapDevNum].position;
		} else {
			hips [hapDevNum].error0 = 
				Vector3.Scale (hips [hapDevNum].desiredPosition, tempNormal) -
				Vector3.Scale (hips [hapDevNum].position, tempNormal);
		}

		hips[hapDevNum].error0 += 
			hips[hapDevNum].contactNormal  * 
			hips[hapDevNum].radius;

		forceField = hips[hapDevNum].q0 * hips[hapDevNum].error0 +
			hips[hapDevNum].q1 * hips[hapDevNum].error1 +
			hips[hapDevNum].q2 * hips[hapDevNum].error2;

		forceField = (forceField.magnitude > maxForceField) ? 
			forceField.normalized * maxForceField :
			forceField;

		ObjectBehaviour.instance.Aforce = forceField.y;

		hips[hapDevNum].error2 = hips[hapDevNum].error1;
		hips[hapDevNum].error1 = hips[hapDevNum].error0;

		HapticPluginImport.SetHapticsForce(myHapticPlugin, hapDevNum, forceField);
	}

	// Thread for haptic device handling
	void HapticThread() 
	{
		while (hapticThreadIsRunning)
		{
			// get left haptic
			for(int i = 0; i < hapticDevices; i++)
			{
				hips[i].position = workspace * HapticPluginImport.GetHapticsPositions(myHapticPlugin, i);
				hips[i].button0 = HapticPluginImport.GetHapticsButtons(myHapticPlugin, i, 1);

				if (hips[i].isColliding){
					SetForceByDesiredPosition(i);
				}
					
				HapticPluginImport.UpdateHapticDevices(myHapticPlugin, i);
			}
		}
	}
		
	// Use this for initialization
	void Start () 
	{
		// inizialization of Haptic Plugin
		Debug.Log("Starting Haptic Devices");
		// check if haptic devices libraries were loaded
		myHapticPlugin = HapticPluginImport.CreateHapticDevices();
		hapticDevices = HapticPluginImport.GetHapticsDetected(myHapticPlugin);
		if (hapticDevices > 0)
		{
			Debug.Log("Haptic Devices Found: " + HapticPluginImport.GetHapticsDetected(myHapticPlugin).ToString());
			hips = new HapticInteractionPoint[hapticDevices];
			for(int haptic = 0; haptic < hapticDevices; haptic++)
				hips[haptic] = (HapticInteractionPoint)ihips[haptic].GetComponent(typeof(HapticInteractionPoint));	
		}
		else
		{
			Debug.Log("Haptic Devices cannot be found");
			Application.Quit();
		}
		// setting the haptic thread
		hapticThreadIsRunning = true;
		myHapticThread = new Thread(HapticThread);
		// set priority of haptic thread
		myHapticThread.Priority = System.Threading.ThreadPriority.Highest;
		// starting the haptic thread
		myHapticThread.Start();
	}

	// Update is called once per frame
	void Update () 
	{
		// Exit application
		if (Input.GetKey(KeyCode.Escape))
		{
			Application.Quit();
		}
	}

	// OnDestroy is called when closing application
	void OnDestroy() {
		// close haptic thread
		EndHapticThread();
		// delete haptic plugin
		HapticPluginImport.DeleteHapticDevices(myHapticPlugin);
		Debug.Log("Application ended correctly");
	}

	// Closes the thread that was created
	void EndHapticThread()
	{
		hapticThreadIsRunning = false;
		Thread.Sleep(100);

		// variables for checking if thread hangs
		bool isHung = false; // could possibely be hung during shutdown
		int timepassed = 0;  // how much time has passed in milliseconds
		int maxwait = 10000; // 10 seconds
		Debug.Log("Shutting down Haptic Thread");
		try
		{
			// loop until haptic thread is finished
			while (myHapticThread.IsAlive && timepassed <= maxwait)
			{
				Thread.Sleep(10);
				timepassed += 10;
			}

			if (timepassed >= maxwait)
			{
				isHung = true;
			}
			// Unity tries to end all threads associated or attached
			// to the parent threading model, if this happens, the 
			// created one is already stopped; therefore, if we try to 
			// abort a thread that is stopped, it will throw a mono error.
			if (isHung)
			{
				Debug.Log("Haptic Thread is hung, checking IsLive State");
				if (myHapticThread.IsAlive)
				{
					Debug.Log("Haptic Thread object IsLive, forcing Abort mode");
					myHapticThread.Abort();
				}
			}
			Debug.Log("Shutdown of Haptic Thread completed.");
		}
		catch (Exception e)
		{
			// lets let the user know the error, Unity will end normally
			Debug.Log("ERROR during OnApplicationQuit: " + e.ToString());
		}
	}
		
}
