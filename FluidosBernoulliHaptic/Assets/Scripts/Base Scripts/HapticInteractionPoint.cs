﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HapticInteractionPoint : MonoBehaviour {

	// establish Haptic Manager and IHIP objects
	public GameObject IHIP;

	// haptic device number
	public int hapticDevice;
	// haptic device variables
	public Vector3 position;
	public bool button0;

	// haptic device variables
	public float radius;
	public Vector3 desiredPosition;

	// hip colliding flag
	public bool isColliding;

	// object in the scene that was hitted
	private Vector3 iHIPposition;
	public Vector3 contactNormal;


	[Header("PID")]
	// stiffness coefficient
	public float q0; // [N/m]
	public float q1; // [N/m]
	public float q2; // [N/m]

	// Error variables
	public Vector3 error0;
	public Vector3 error1;
	public Vector3 error2;


	// Singleton instance
//	public static HapticInteractionPoint instance; 

	// Called when the script instance is being loaded
	void Awake() {
		// get haptic device to be used
		hapticDevice = 0;
		position = new Vector3(0, 0, 0);
		button0 = false;
		isColliding = false;
	}

	// Use this for initialization
	void Start () {
		// contactNormal*IHIP.transform.localScale.x/2.0f es el radio del IHIP
		radius = IHIP.transform.localScale.x / 2.0f;

		q0 = 50.0f;
		q1 = 25.0f;
		q2 = 10.0f;

		desiredPosition = new Vector3 (0, 0, 0);
		error0 = new Vector3 (0, 0, 0);
		error1 = new Vector3 (0, 0, 0);
		error2 = new Vector3 (0, 0, 0);
	}

	// Update is called once per frame
	void Update () {
		if(isColliding)
		{
			IHIP.transform.position = desiredPosition; 
		}else{
			IHIP.transform.position = position;	
		}
		this.transform.position = position;
	}
		

	void OnCollisionStay(Collision collision)
	{
		if (collision.gameObject.name == "Object") {
			// Si ya estoy adentro del objecto separation < 0.

			Collider hipCollider = this.GetComponent<Collider> ();
			if(hipCollider.bounds.Intersects(collision.collider.bounds)){
				isColliding = true;
				contactNormal = collision.contacts [0].normal;
				desiredPosition = collision.contacts [0].point
					+ (contactNormal* Mathf.Abs(collision.contacts [0].separation)); 	

				if(button0){
					desiredPosition = new Vector3 (0, desiredPosition.y, 0);
				}

			}else{
				isColliding = false;
			}
		}
	}

	void OnCollisionExit(Collision collision)
	{
		if (collision.gameObject.name == "Object") {
			isColliding = false;
//			ObjectBehaviour.instance.epsilonHeight = 0.0005f;
		}
	}

}

