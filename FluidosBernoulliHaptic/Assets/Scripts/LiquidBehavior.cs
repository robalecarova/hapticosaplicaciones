﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LiquidBehavior : MonoBehaviour {

	// Singleton Instantiation

	public static LiquidBehavior instance; 
	public ToggleGroup densityLiquidTG;

	void Awake()
	{
		instance = this;
	}
		
	// Use this for initialization
	void Start () {
		
		// Change the liquid transparency
		Renderer rend = LiquidBehavior.instance.GetComponent<Renderer> ();
		rend.material.color = Color.cyan;

		Color currentColor = rend.material.color;

		float alphaVal = (1000.0f - ObjectBehaviour.instance.densityLiquid.minValue) /
			(ObjectBehaviour.instance.densityLiquid.maxValue - ObjectBehaviour.instance.densityLiquid.minValue);

		//Adjust Alpha value to never be greater than half value and add a shift
		currentColor.a = ( alphaVal* 0.5f ) + 0.25f;  // New range (0.25,0.75)
		rend.material.color = currentColor;
		
	}
}
