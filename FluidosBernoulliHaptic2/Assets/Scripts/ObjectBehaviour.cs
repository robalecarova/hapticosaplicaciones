﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class ObjectBehaviour : MonoBehaviour {

	// Object Parameters
	public float mass;
	public float weight;
	public float Bforce;
	public float Aforce;
	public float height2;
	float volume;
	float height;
	float lVolume;
	float lastHeight;

	// Text output
	public Text massText;
	public Text weightText;
	public Text volumeObjectText;
	public Text immerseVolumeText;
	public Text heightText;
	public Text bForceText;
	public Text aForceText;

	// Value input
	public Slider edge;
	public Slider densityObject;
	public Slider densityLiquid;

	public InputField edgeIF;
	public InputField densityObjectIF;
	public InputField densityLiquidIF;

	public ToggleGroup densityObjectTG;
	public ToggleGroup vectorsTG;

	// Singleton Instantiation
	public static ObjectBehaviour instance; 

	// Arrows

	public GameObject arrowB;
	public GameObject arrowW;
	public GameObject arrowA;

	// Other values
	float gravityConstant = 9.81f;
	public float liquidVolume; // Es escala, no unidades

	float minForce;
	float maxForce ;
	float maxRangeTopArrow;

	Vector3 objectPosition;
	Vector3 liquidPosition;
	Vector3 liquidScale;

	Vector3 arrowBPosition;
	Vector3 arrowBScale;

	Vector3 arrowWPosition;
	Vector3 arrowWScale;

	Vector3 arrowAPosition;
	Vector3 arrowAScale;

    public int objectId;
    Vector3 desplazar;

    // It will change the mesh. 
    // 0 - will be a cube
    // 1 - it will be a sphere
    //	public Mesh[] possibleMeshes = new Mesh[2];

    // Materials will be inputed in the Inspector View
    public Material[] possibleMaterials;

	// Actualizar los valores basado en la velocidad del procesamiento del fixedUpdate
	int counterUpdateRefresh = 0;

	void Awake(){
		instance = this;
	}

	// Use this for initialization
	void Start () {

		// Start the meshes and the materials
		//		GameObject gameObject = GameObject.CreatePrimitive (PrimitiveType.Cube);
		//		possibleMeshes[0] = gameObject.GetComponent<MeshFilter>().sharedMesh;
		//		GameObject.Destroy(gameObject);
		//
		//		gameObject = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		//		possibleMeshes[1] = gameObject.GetComponent<MeshFilter>().sharedMesh;
		//		GameObject.Destroy(gameObject);
		//
		this.GetComponent<Renderer> ().material = possibleMaterials [0];

		minForce = 0.0f;
		maxForce = 10.0f;
		maxRangeTopArrow = 0.5f;

		// Set the values
		lastHeight = 0.0f;
		liquidVolume = 0.5f;

		edge.maxValue = 4.5f;
		edge.minValue = 3.5f;
		edge.value = 4.0f;

		edgeIF.text = edge.value.ToString();

		densityObject.maxValue = 10000.0f;
		densityObject.minValue = 200.0f;
		densityObject.value = 8920.0f;
		densityObjectIF.text = densityObject.value.ToString();

		densityLiquid.maxValue = 15000.0f;
		densityLiquid.minValue = 500.0f;
		densityLiquid.value = 1000.0f;
		densityLiquidIF.text = densityLiquid.value.ToString();

		height = 0.0f;
		height2 = 0.0f;
		Bforce = 0.0f;
		Aforce = 0.0f;

		lVolume = 0.0f;
		immerseVolumeText.text = lVolume.ToString();


		objectPosition = new Vector3 (0.0f, (edge.value/20.0f ), 0.0f);
        desplazar = new Vector3(0.0f, 0.0f, 0.0f);
        liquidPosition = new Vector3 (0.0f, -0.25f, 0.0f);
		liquidScale = new Vector3 (1.0f, 0.5f, 1.0f);

		arrowWScale = new Vector3 (0.250f, 0.1f, 0.10f);
		arrowBScale = new Vector3 (0.250f, 0.1f, 0.10f);
		arrowAScale = new Vector3 (0.250f, 0.1f, 0.10f);

		arrowWPosition = new Vector3 (0.10f, 0.0f, -0.50f);
		arrowBPosition = new Vector3 (-0.045f, 0.0f, -0.50f);
		arrowAPosition = new Vector3 (-0.75f, 0.0f, -0.50f);

        epsilonHeight = 0.0001f;
        //epsilonHeight = 0.1f;

        objectId = 0;
    }

	// Update is called once per frame
	void Update () {

        HapticNativePlugin.TranslateObject(objectId, desplazar * Time.deltaTime);
        transform.Translate(desplazar * Time.deltaTime);        

        // Liquid behaviour                
        LiquidBehavior.instance.transform.localPosition = liquidPosition;
		LiquidBehavior.instance.transform.localScale = liquidScale;

		// Arrows behaviour
		Toggle temp = vectorsTG.ActiveToggles ().FirstOrDefault ();
			
		
	
		if (counterUpdateRefresh % 10 == 0) {
            if (temp.name == "VectorON")
            {
                ComputeArrows();
                arrowW.SetActive(true);
                arrowW.transform.localScale = arrowWScale;
                arrowW.transform.position = arrowWPosition;
                arrowB.SetActive(true);
                arrowB.transform.localScale = arrowBScale;
                arrowB.transform.position = arrowBPosition;

                arrowA.SetActive(true);
                arrowA.transform.localScale = arrowAScale;
                arrowA.transform.position = arrowAPosition;

            }
            else
            {
                arrowB.SetActive(false);
                arrowW.SetActive(false);
                arrowA.SetActive(false);
            }

            volumeObjectText.text = (volume * 1000000).ToString("F3"); // Lo paso de m^3 a cm^3
			massText.text = mass.ToString("F3");
			weightText.text = weight.ToString("F3");
			heightText.text = (height*100).ToString ("F3"); //  Lo paso  de m a cm
			immerseVolumeText.text = (lVolume * 1000000).ToString("F3");  // Lo paso de m^3 a cm^3
			bForceText.text = Bforce.ToString ("F3");
			
            // Ajustes para que no vibren tanto los valores de Fuerza
            float tempValue = edge.value / 100.0f;
            Aforce = (height > tempValue) ? Bforce - weight: Aforce;
            aForceText.text = (Aforce).ToString ("F3");
		}

        // Actualizacion del contador para actualziacion de valores graficos
        counterUpdateRefresh++;
	}
		
	public float epsilonHeight;
    public float temp;
	void FixedUpdate(){

		// Actualización de unidades
		float tempValue = edge.value/100.0f; // Pasamos a metros para todos los calculos
		volume = tempValue * tempValue * tempValue;
		mass = densityObject.value * volume ;
		weight = mass * gravityConstant;

        temp = Aforce + weight;
        height = temp / (densityLiquid.value * tempValue * tempValue * gravityConstant);

        // Compute immerse object volume
        lVolume = (height > tempValue) ? tempValue * tempValue * tempValue : tempValue * tempValue * height;

        // Si la altura ya paso el tamaño el objeto
        Bforce = densityLiquid.value * lVolume * gravityConstant;

        temp = temp - Bforce;

        if (Mathf.Abs (temp) > epsilonHeight) {
			if (temp > epsilonHeight) {
				height = lastHeight + epsilonHeight;
			} else {
				height = lastHeight - epsilonHeight;
			}
		}
			
		// The Object cannot sink more than the bottom layer of the fish tank
		height = ( height*100.0f > 5f) ? 0.05f : height;
		lastHeight = height; // Esto es para hacer que no vibre tanto


		/******************/
		// Graphic section
		/******************/
		objectPosition.y = (tempValue * 5.0f) - (height*10.0f);
        desplazar = objectPosition - transform.localPosition;

        // The 0.5f factor is to consider the object volume already inside the liquid
        // The 10.0 factor is to change from cm to scale
        temp = lVolume * 1000 + liquidVolume;

//		liquidPosition.y = -0.25f + (0.5f*(temp-0.5));
		liquidScale.y = temp;

	}

	void ComputeArrows(){
		// compute arrows
		// Weight
		arrowWScale.x = (weight - minForce)/ (maxForce - minForce); // normalize value (0,1)
		arrowWScale.x = (arrowWScale.x * maxRangeTopArrow) + 0.01f; // Range (0.01, maxRangeTopArrow)

		// it is divided by two, to get the object's center
		arrowWPosition.y = this.transform.position.y - (arrowWScale.x / 2.0f);
		arrowWPosition.x = this.transform.position.x;

		//Buoyancy
		arrowBScale.x = (Bforce - minForce)/ (maxForce - minForce); // normalize value (0,1)
		arrowBScale.x = (arrowBScale.x * maxRangeTopArrow) + 0.01f; // Range (0.01, maxRangeTopArrow)

		// it is divided by two, to get the object's center
		arrowBPosition.y = this.transform.position.y + (arrowBScale.x / 2.0f);
		arrowBPosition.x = this.transform.position.x;

		// Applied force
		arrowAScale.x = ((Aforce/2) - minForce)/ (maxForce - minForce); // normalize value (0,1)
		arrowAScale.x = (arrowAScale.x * maxRangeTopArrow) + 0.01f; // Range (0.01, maxRangeTopArrow)

		// it is divided by two, to get the object's center
		arrowAPosition.y = this.transform.position.y - (arrowAScale.x / 2.0f);
		arrowAPosition.x = this.transform.position.x - 0.2f;

	}
}