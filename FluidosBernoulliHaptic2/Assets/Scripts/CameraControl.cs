﻿using UnityEngine;
using UnityEngine.UI;

public class CameraControl : MonoBehaviour {


    private Vector3 previousPosition, originalPosition, previousRotation, epsilonVector;
    float rotationX, rotationY;

    private Transform HIP;

    private void Start()
    {
        HIP = GameObject.Find("HIP").transform;        
        previousPosition = HIP.position;
        originalPosition = HIP.position;

        previousRotation = this.transform.rotation.eulerAngles;

        HIP.localPosition = new Vector3(0f, 0f, 0f);

        UpdateHapticPosition();
    }

	// Update is called once per frame
	void Update () {

        if (Input.GetKey("escape"))        
            Application.Quit();
        
        UpdateHapticPosition();
        UpdateHapticRotation();

    }

    private Vector3 device, proxy, force;

    private void FixedUpdate()
    {

        // si exite una diferencia entre las posiciones ideales y real del haptico, significa que se genera una 
        device = HapticNativePlugin.GetDevicePosition();
        proxy = HapticNativePlugin.GetProxyPosition();
        force = (device - proxy) * HapticNativePlugin.kSpring*20;

        ObjectBehaviour.instance.Aforce = -force.y;

    }


    private void UpdateHapticPosition()
    {
        if (HIP == null)
            return;
        if (previousPosition != HIP.position)
        {
            HapticNativePlugin.SetHapticPosition((HIP.position - originalPosition) / 0.5f);
        }
        previousPosition = HIP.position;
    }

    private void UpdateHapticRotation()
    {
        if (previousRotation != this.transform.rotation.eulerAngles)
        {
            HapticNativePlugin.SetHapticRotation(this.transform.rotation.eulerAngles);
        }
        previousPosition = this.transform.rotation.eulerAngles;
    }

}
