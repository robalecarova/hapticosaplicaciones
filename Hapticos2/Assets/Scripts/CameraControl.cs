﻿using UnityEngine;
using UnityEngine.UI;

public class CameraControl : MonoBehaviour {


    private Vector3 previousPosition, originalPosition, previousRotation, epsilonVector;
    float rotationX, rotationY;

    private Transform HIP;

    private void Start()
    {
        HIP = GameObject.Find("HIP").transform;        
        previousPosition = HIP.position;
        originalPosition = HIP.position;

        previousRotation = this.transform.rotation.eulerAngles;

        HIP.localPosition = new Vector3(0f, 0f, 0.5f);

        UpdateHapticPosition();
    }

	// Update is called once per frame
	void Update () {

        if (Input.GetKey("escape"))        
            Application.Quit();
        

        float scroll = Input.GetAxis("Mouse ScrollWheel");
		transform.Translate(0, 0f, scroll * 2f, Space.Self);

		if (Input.GetMouseButton (1)) {
			rotationX = Input.GetAxis ("Mouse X") * 150f * Time.deltaTime;
			rotationY = Input.GetAxis ("Mouse Y") * 150f * Time.deltaTime;
			transform.Rotate(-rotationY, rotationX, 0);
		}
		else if (Input.GetMouseButton (2)) {
			var xMove = Input.GetAxis ("Mouse X") * -10f * Time.deltaTime;
			var yMove = Input.GetAxis ("Mouse Y") * -10f * Time.deltaTime;
			transform.Translate(xMove, yMove, 0f, Space.Self);
		}

        if (Input.GetKey(KeyCode.UpArrow))
            transform.Translate(0f, 0f, 1f * Time.deltaTime);
        else if (Input.GetKey(KeyCode.DownArrow))
            transform.Translate(0f, 0f, -1f * Time.deltaTime);
        else if (Input.GetKey(KeyCode.LeftArrow))
            transform.Translate(-1f * Time.deltaTime, 0f, 0f);
        else if (Input.GetKey(KeyCode.RightArrow))
            transform.Translate(1f * Time.deltaTime, 0f, 0f);

       
        UpdateHapticPosition();
        UpdateHapticRotation();

    }    

    private void UpdateHapticPosition()
    {
        if (HIP == null)
            return;
        if (previousPosition != HIP.position)
        {
            HapticNativePlugin.SetHapticPosition((HIP.position - originalPosition) / 0.5f);
        }
        previousPosition = HIP.position;
    }

    private void UpdateHapticRotation()
    {
        if (previousRotation != this.transform.rotation.eulerAngles)
        {
            HapticNativePlugin.SetHapticRotation(this.transform.rotation.eulerAngles);
        }
        previousPosition = this.transform.rotation.eulerAngles;
    }

}
