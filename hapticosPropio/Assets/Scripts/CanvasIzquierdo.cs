﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasIzquierdo : MonoBehaviour {

    public Toggle massToggle, steepnessToggle, frictionToggle;
    public Slider steepnessSlider, SFslider, massSlider;
    public InputField steepnessText, SFtext, massText;
    public GameObject rampa, caja, camaraPrincipal, flechaRotar;
    public Text dynamicText;
    public Camera minimap;

    private float dynFrictionValue, statFrictionValue, steepnessValue, massValue;    

    private Collider coll;
    private Rigidbody rb;
    private CameraControl parametros;

    // Use this for initialization
    void Start () {

        coll = caja.GetComponent<Collider>();
        rb = caja.GetComponent<Rigidbody>();
        parametros = camaraPrincipal.GetComponent<CameraControl>();

        statFrictionValue = 0.5f;
        dynFrictionValue = statFrictionValue * 0.7f;    

        coll.material.staticFriction = statFrictionValue;
        coll.material.dynamicFriction = dynFrictionValue;

        massValue = 1.0f;
        steepnessValue = 0;

        steepnessText.text = steepnessValue.ToString("F2") + "°";
        steepnessSlider.value = steepnessValue;
        SFtext.text = statFrictionValue.ToString("F3");
        SFslider.value = statFrictionValue;
        dynamicText.text = dynFrictionValue.ToString("F3");
        massText.text = massValue.ToString("F3") + "kg";
        massSlider.value = massValue;
        rb.mass = massValue;

        parametros.SetFuerzaFriccion(statFrictionValue, dynFrictionValue, massValue, steepnessValue);        
    }

    // Invoked when the value of the slider changes.
    public void SliderSteepness()
    {
        steepnessValue = steepnessSlider.value;
        rampa.transform.localEulerAngles = new Vector3(0,0, -steepnessValue);
        steepnessText.text = steepnessValue.ToString("F2") + "°";
        parametros.SetFuerzaFriccion(statFrictionValue, dynFrictionValue, massValue, steepnessValue);
        flechaRotar.transform.localEulerAngles = new Vector3(0, 0, -steepnessValue);

        minimap.transform.localEulerAngles = new Vector3(0, 0, -steepnessValue);
    }

    public void SetValueSteepness(string newText)
    {
        
        float temp = float.Parse(newText);
        temp = (temp > steepnessSlider.maxValue) ? steepnessSlider.maxValue : temp;
        temp = (temp < steepnessSlider.minValue) ? steepnessSlider.minValue : temp;

        steepnessSlider.value = temp;
        SliderSteepness();
    }


    public void SliderSF()
    {
        statFrictionValue = SFslider.value;
        dynFrictionValue = statFrictionValue * 0.7f;        

        coll.material.staticFriction = statFrictionValue;
        coll.material.dynamicFriction = dynFrictionValue;

        SFtext.text = statFrictionValue.ToString("F3");
        SFslider.value = statFrictionValue;

        dynamicText.text = dynFrictionValue.ToString("F3");

        parametros.SetFuerzaFriccion(statFrictionValue, dynFrictionValue, massValue, steepnessValue);
    }

    public void SetValueSF(string newText)
    {

        float temp = float.Parse(newText);
        temp = (temp > SFslider.maxValue) ? SFslider.maxValue : temp;
        temp = (temp < SFslider.minValue) ? SFslider.minValue : temp;

        SFslider.value = temp;
        SliderSF();
    }


    public void SliderMass()
    {
        massValue = massSlider.value;
        rb.mass = massValue;
        massText.text = massValue.ToString("F3") + "kg";
        parametros.SetFuerzaFriccion(statFrictionValue, dynFrictionValue, massValue, steepnessValue);
    }

    public void SetValueMass(string newText)
    {

        float temp = float.Parse(newText);
        temp = (temp > massSlider.maxValue) ? massSlider.maxValue : temp;
        temp = (temp < massSlider.minValue) ? massSlider.minValue : temp;

        massSlider.value = temp;
        SliderMass();
    }

    public void MassToggle(bool flag)
    {
        if (flag)        
            massToggle.GetComponent<LayoutElement>().minHeight = 150;
        else
            massToggle.GetComponent<LayoutElement>().minHeight = 60;

    }


    public void SteepnessToggle(bool flag)
    {
        if (flag)
            steepnessToggle.GetComponent<LayoutElement>().minHeight = 150;
        else
            steepnessToggle.GetComponent<LayoutElement>().minHeight = 60;
    }

    public void FrictionToggle(bool flag)
    {
        if (flag)
            frictionToggle.GetComponent<LayoutElement>().minHeight = 150;
        else
            frictionToggle.GetComponent<LayoutElement>().minHeight = 60;
    }





}
