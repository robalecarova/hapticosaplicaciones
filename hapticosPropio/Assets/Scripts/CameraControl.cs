﻿using UnityEngine;
using UnityEngine.UI;

public class CameraControl : MonoBehaviour {

    public float refresh;

    float factorFuerza = 4.0f;
     
    private float rotationX = 0f;
	private float rotationY = 0f;
    private readonly float gravity = 9.81f;

    private readonly float limitMin = -3.8f;
    private readonly float limitMax = 4.8f;

    private readonly float maxForce = 9.81f;

    public float fuerzaFriccionEstatica, fuerzaFriccionDinamica, peso, fuerzaNormal, fuerzaResbalar, sumaFuerzasX;    
    private Vector3 previousPosition, originalPosition, previousRotation, epsilonVector;

    private Transform HIP;
    private Vector3 force, device, proxy, desplazar;

    public Text af, nf, ff, sf, w;

    public GameObject caja;
    //Flechas
    public GameObject fN, fW, fF, fA;

    private bool isMoving;

    private float sign;

    public void SetFuerzaFriccion(float staticCoefficient, float dynamicCoefficient, float mass, float steepness)
    {
        peso = gravity * mass;
        fuerzaNormal = peso * Mathf.Cos(Mathf.Deg2Rad*steepness);
        fuerzaResbalar = peso * Mathf.Sin(Mathf.Deg2Rad * steepness);
        fuerzaFriccionEstatica = staticCoefficient * fuerzaNormal;
        fuerzaFriccionDinamica = dynamicCoefficient * fuerzaNormal;

        factorFuerza = (staticCoefficient < 0.001f) ? 100.0f : 4.0f;
     
        nf.text = fuerzaNormal.ToString("F2")+"N";
        w.text = peso.ToString("F2") + "N";


        // Ajusto fuerzas a las dimensiones de la escena
        float temp1 = peso/ maxForce;
        Vector3 temp2 = fW.transform.localScale;
        temp2.y = 0.003f * temp1; //0.003 es la escala del ambiente
        fW.transform.localScale = temp2;

        Vector3 temp3 = fW.transform.localPosition;
        temp3.y = -temp2.y * 100;
        fW.transform.localPosition = temp3;

        temp1 = fuerzaNormal / maxForce;
        temp2 = fN.transform.localScale;
        temp2.y = 0.003f * temp1;
        fN.transform.localScale = temp2;

        temp3 = fN.transform.localPosition;
        temp3.y = temp2.y * 100;
        fN.transform.localPosition = temp3;

    }

    private void Start()
    {
        HIP = GameObject.Find("HIP").transform;        
        previousPosition = HIP.position;
        originalPosition = HIP.position;

        previousRotation = this.transform.rotation.eulerAngles;

        HIP.localPosition = new Vector3(0f, 0f, 0.5f);
        force = new Vector3(0f, 0f, 0f);
        desplazar = new Vector3(0f, 0f, 0f);

        epsilonVector = new Vector3(0.05f, 0f, 0f);

        isMoving = false;

        refresh = Time.deltaTime;

        SetFuerzaFriccion(0.5f, 0.35f, 1, 0);

        UpdateHapticPosition();
    }

	// Update is called once per frame
	void Update () {


        // Funcionalidades extra - Camara
        if (Input.GetKey("escape"))        
            Application.Quit();
       
        float scroll = Input.GetAxis("Mouse ScrollWheel");
		transform.Translate(0, 0f, scroll * 2f, Space.Self);

		if (Input.GetMouseButton (1)) {
			rotationX = Input.GetAxis ("Mouse X") * 150f * refresh;
			rotationY = Input.GetAxis ("Mouse Y") * 150f * refresh;
			transform.Rotate(-rotationY, rotationX, 0);
		}
		else if (Input.GetMouseButton (2)) {
			var xMove = Input.GetAxis ("Mouse X") * -10f * refresh;
			var yMove = Input.GetAxis ("Mouse Y") * -10f * refresh;
			transform.Translate(xMove, yMove, 0f, Space.Self);
		}

        if (Input.GetKey(KeyCode.UpArrow))
            transform.Translate(0f, 0f, 1f * refresh);
        else if (Input.GetKey(KeyCode.DownArrow))
            transform.Translate(0f, 0f, -1f * refresh);
        else if (Input.GetKey(KeyCode.LeftArrow))
            transform.Translate(-1f * refresh, 0f, 0f);
        else if (Input.GetKey(KeyCode.RightArrow))
            transform.Translate(1f * refresh, 0f, 0f);


        // Desplazamiento - Tiene en cuenta los rieles - Limites Min y Max

        if (desplazar.magnitude > 0)
        {
            // Me muevo a la izquierda
            if ( desplazar.x < 0)
                if(caja.transform.localPosition.x > limitMin)
                {
                    HapticNativePlugin.TranslateObject(HapticNativePlugin.touchables[caja.name], desplazar * refresh);
                    caja.transform.Translate(desplazar * refresh);
                }else
                    isMoving = false;
            else if (desplazar.x > 0)
            {
                if (caja.transform.localPosition.x < limitMax)
                {
                    HapticNativePlugin.TranslateObject(HapticNativePlugin.touchables[caja.name], desplazar * refresh);
                    caja.transform.Translate(desplazar * refresh);
                }
                else
                    isMoving = false;
            }
                
        }

        sign = Mathf.Sign(sumaFuerzasX);
        float temp;

        // Si se esta moviendo hago esto para la fuerza de friccion
        temp = (isMoving) ? fuerzaFriccionDinamica * sign * -1.0f : Mathf.Min(fuerzaFriccionEstatica, Mathf.Abs(sumaFuerzasX)) * sign * -1.0f;          
        ff.text = temp.ToString("F2") + "N";

        // Ajusto fuerzas a las dimensiones de la escena
        // Inicio
        float temp1 = temp / maxForce; 
        Vector3 temp2 = fF.transform.localScale;
        temp2.y = 0.003f * temp1;
        fF.transform.localScale = temp2;

        Vector3 temp3 = fF.transform.localPosition;
        temp3.x = temp2.y * 100;
        fF.transform.localPosition = temp3;

        temp1 = force.x / maxForce;
        temp2 = fA.transform.localScale;
        temp2.y = 0.003f * temp1;
        fA.transform.localScale = temp2;

        temp3 = fA.transform.localPosition;
        temp3.x = temp2.y * 100;
        fA.transform.localPosition = temp3;
        // Fin


        af.text = force.ToString("F1");
        sf.text = (sumaFuerzasX + temp).ToString("F2");

        UpdateHapticPosition();
        UpdateHapticRotation();
    }

    private void FixedUpdate()
    {
       
        // si exite una diferencia entre las posiciones ideales y real del haptico, significa que se genera una 
        device = HapticNativePlugin.GetDevicePosition();
        proxy = HapticNativePlugin.GetProxyPosition();
        force = (device - proxy) * HapticNativePlugin.kSpring*factorFuerza;

        sumaFuerzasX = force.x - fuerzaResbalar;
        sign = Mathf.Sign(sumaFuerzasX);

        desplazar = new Vector3(0f, 0f, 0f);

        float temp;
        if (isMoving && Mathf.Abs(sumaFuerzasX) > fuerzaFriccionDinamica)
        {
            temp = fuerzaFriccionDinamica * sign * -0.5f;
            desplazar.x = (sumaFuerzasX + temp) / (HapticNativePlugin.kSpring);
        }
        else if (Mathf.Abs(sumaFuerzasX) > fuerzaFriccionEstatica)
        {
            temp = fuerzaFriccionEstatica * sign * -1.0f;
            desplazar.x = (sumaFuerzasX + temp) / (HapticNativePlugin.kSpring);
            isMoving = true;
        }
        else
            isMoving = false;
        
    }

    private void UpdateHapticPosition()
    {
        if (HIP == null)
            return;
        if (previousPosition != HIP.position)
            HapticNativePlugin.SetHapticPosition((HIP.position - originalPosition) / 0.5f);
        previousPosition = HIP.position;
    }

    private void UpdateHapticRotation()
    {
        if (previousRotation != this.transform.rotation.eulerAngles)
            HapticNativePlugin.SetHapticRotation(this.transform.rotation.eulerAngles);
        previousPosition = this.transform.rotation.eulerAngles;
    }

}
