﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HapticInteractionPoint : MonoBehaviour {

	// establish Haptic Manager and IHIP objects
	public GameObject hapticManager;
	public GameObject IHIP;

	// get haptic device information from the haptic manager
	private HapticManager myHapticManager;

	// haptic device number
	public int hapticDevice;
	// haptic device variables
	private Vector3 position;
	public bool button0;
	private Rigidbody rigidBody;

	// haptic device variables
	public float radius;
	public Vector3 desiredPosition;

	// hip colliding flag
	public bool isColliding;

	// object in the scene that was hitted
	private float objectMass;
	private string objectColliding;
	private Vector3 HIPCollidingPosition;


	[Header("Attraction Force")]
	// stiffness coefficient
	public float Kp; // [N/m]

	[Header("Damping Factors")]
	// damping term
	public float Kv; // [N/m]

	// Singleton instance
	public static HapticInteractionPoint instance; 

	// MaxValues
	float maxKv;

	// Called when the script instance is being loaded
	void Awake() {
		position = new Vector3(0, 0, 0);
		button0 = false;
		rigidBody = GetComponent<Rigidbody>();
		isColliding = false;

		instance = this;
	}

	// Use this for initialization
	void Start () {
		myHapticManager = (HapticManager)hapticManager.GetComponent(typeof(HapticManager));
		radius = IHIP.GetComponent<Renderer> ().bounds.extents.magnitude / 2.0f;

		// set haptic device mass
		rigidBody.mass = 0.0f;

		Kp = 10;
		Kv = 20; // [N/m]
	}

	// Update is called once per frame
	void Update () {

		// get haptic device to be used
		hapticDevice = 0;

		// get haptic device variables
		position = myHapticManager.GetPosition(hapticDevice);
		button0 = myHapticManager.GetButtonState(hapticDevice, 0);

		// update positions of HIP and IHIP
		if (isColliding) {
			if(button0){
				HIPCollidingPosition = desiredPosition;
				HIPCollidingPosition.z = 0.0f;
				IHIP.transform.position = HIPCollidingPosition;
			} else
				IHIP.transform.position = desiredPosition;
			this.transform.position = position;

			Debug.Log (position);
		} else {
			IHIP.transform.position = position;
			this.transform.position = position;

			Seesaw.instance.forceL = 0.0f;
		}

		// update damping factors
		maxKv = myHapticManager.GetHapticDeviceInfo(hapticDevice, 6);
		Kv = (Kv > maxKv) ? maxKv : Kv;
	}

	void OnCollisionStay(Collision collision)
	{
		if (collision.gameObject.name == "tabla") {
			isColliding = true;
			desiredPosition = position + (collision.contacts [0].normal * Mathf.Abs (collision.contacts [0].separation));
		}
	}


	void OnCollisionExit(Collision collision)
	{
		if (collision.gameObject.name == "tabla") {
			isColliding = false;
		}
	}

}

