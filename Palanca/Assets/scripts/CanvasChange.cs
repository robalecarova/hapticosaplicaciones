﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasChange : MonoBehaviour {
	public Canvas instrucciones;

	void Start(){
		instrucciones.enabled = false;
	}

	public void ShowInstrucciones(){
		instrucciones.enabled = true;
	}

	public void DisableInstrucciones(){
		instrucciones.enabled = false;
	}

}
