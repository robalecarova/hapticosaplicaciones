﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class HapticManager : MonoBehaviour 
{
	// plugin import
	private IntPtr myHapticPlugin;
	// haptic thread
	private Thread myHapticThread;
	// a flag to indicate if the haptic simulation currently running
	private bool hapticThreadIsRunning;
	// haptic devices in the scene
	// haptic workspace
	float workspace = 300.0f;
	// number of haptic devices detected
	private int hapticDevices;

	// Left haptic device
	private Vector3 positionLeft;
	private Vector3 tempDesiredPosLeft;
	private bool button0Left;
	// I will use the first haptic device for left haptic
	int i = 0;


	// Right haptic device
	private Vector3 positionRight;
	private Vector3 tempDesiredPosRight;
	private bool button0Right;
//	 I will use the second haptic device for right haptic
	int j = 1;

	// Use this for initialization
	void Start () 
	{
		// inizialization of Haptic Plugin
		Debug.Log("Starting Haptic Devices");
		// check if haptic devices libraries were loaded
		myHapticPlugin = HapticPluginImport.CreateHapticDevices();
		hapticDevices = HapticPluginImport.GetHapticsDetected(myHapticPlugin);
//		if (hapticDevices > 1) // Este es el que se necesita para tener dos hapticos o cerrar el ambiente
		if (hapticDevices > 0)
		{
			Debug.Log("Haptic Devices Found: " + HapticPluginImport.GetHapticsDetected(myHapticPlugin).ToString());
		}
		else
		{
			Debug.Log("Haptic Devices cannot be found");
			Application.Quit();
		}
		// setting the haptic thread
		hapticThreadIsRunning = true;
		myHapticThread = new Thread(HapticThread);
		// set priority of haptic thread
		myHapticThread.Priority = System.Threading.ThreadPriority.Highest;
		// starting the haptic thread
		myHapticThread.Start();
	}

	// Update is called once per frame
	void Update () 
	{
		// Exit application
		if (Input.GetKey(KeyCode.Escape))
		{
			Application.Quit();
		}
	}

	// OnDestroy is called when closing application
	void OnDestroy() {
		// close haptic thread
		EndHapticThread();
		// delete haptic plugin
		HapticPluginImport.DeleteHapticDevices(myHapticPlugin);
		Debug.Log("Application ended correctly");
	}

	// Thread for haptic device handling
	void HapticThread() 
	{
		while (hapticThreadIsRunning)
		{
			// get left haptic
			positionLeft = workspace * HapticPluginImport.GetHapticsPositions(myHapticPlugin, i);
			button0Left = HapticPluginImport.GetHapticsButtons(myHapticPlugin, i, 1);

			// get Right haptic
			positionRight = workspace * HapticPluginImport.GetHapticsPositions(myHapticPlugin, j);
			button0Right = HapticPluginImport.GetHapticsButtons(myHapticPlugin, j, 1);

			if (button0Left) {
				HapticInteractionPoint.instance.desiredPosition.z = 0.0f;
			}

			if (button0Right) {
				HapticInteractionPoint2.instance.desiredPosition.z = 0.0f;
			}

			float temp;

			if (HapticInteractionPoint.instance.isColliding && HapticInteractionPoint2.instance.isColliding) {
				temp = HapticInteractionPoint.instance.desiredPosition.x;
				HapticInteractionPoint.instance.desiredPosition.x = (temp > -1.0f) ? -1.0f : temp;

				temp = HapticInteractionPoint2.instance.desiredPosition.x;
				HapticInteractionPoint2.instance.desiredPosition.x = (temp < 1.0f) ? 1.0f : temp;

				SetForceByDesiredPosition(i,HapticInteractionPoint.instance.desiredPosition);
				SetForceByDesiredPosition(j,HapticInteractionPoint2.instance.desiredPosition);
			} else {
				if (HapticInteractionPoint.instance.isColliding) {
					temp = HapticInteractionPoint.instance.desiredPosition.x;
					HapticInteractionPoint.instance.desiredPosition.x = (temp > -1.0f) ? -1.0f : temp;

					SetForceByDesiredPosition(i,HapticInteractionPoint.instance.desiredPosition);
				}


				if (HapticInteractionPoint2.instance.isColliding) {
					temp = HapticInteractionPoint2.instance.desiredPosition.x;
					HapticInteractionPoint2.instance.desiredPosition.x = (temp < 1.0f) ? 1.0f : temp;

					SetForceByDesiredPosition(j,HapticInteractionPoint2.instance.desiredPosition);
				}

			}

			HapticPluginImport.UpdateHapticDevices(myHapticPlugin, i);
			HapticPluginImport.UpdateHapticDevices(myHapticPlugin, j);
		}
	}

	// Closes the thread that was created
	void EndHapticThread()
	{
		hapticThreadIsRunning = false;
		Thread.Sleep(100);

		// variables for checking if thread hangs
		bool isHung = false; // could possibely be hung during shutdown
		int timepassed = 0;  // how much time has passed in milliseconds
		int maxwait = 10000; // 10 seconds
		Debug.Log("Shutting down Haptic Thread");
		try
		{
			// loop until haptic thread is finished
			while (myHapticThread.IsAlive && timepassed <= maxwait)
			{
				Thread.Sleep(10);
				timepassed += 10;
			}

			if (timepassed >= maxwait)
			{
				isHung = true;
			}
			// Unity tries to end all threads associated or attached
			// to the parent threading model, if this happens, the 
			// created one is already stopped; therefore, if we try to 
			// abort a thread that is stopped, it will throw a mono error.
			if (isHung)
			{
				Debug.Log("Haptic Thread is hung, checking IsLive State");
				if (myHapticThread.IsAlive)
				{
					Debug.Log("Haptic Thread object IsLive, forcing Abort mode");
					myHapticThread.Abort();
				}
			}
			Debug.Log("Shutdown of Haptic Thread completed.");
		}
		catch (Exception e)
		{
			// lets let the user know the error, Unity will end normally
			Debug.Log("ERROR during OnApplicationQuit: " + e.ToString());
		}
	}

	public int GetHapticDevicesFound()
	{
		return hapticDevices;
	}

	public Vector3 GetPosition(int numHapDev)
	{
		if (numHapDev == 0) 
			return positionLeft;
		return	positionRight;
		
	}

	public bool GetButtonState(int numHapDev, int button)
	{
		if (numHapDev == 0) 
			return button0Left;
		return button0Right;
	}

	public float GetHapticDeviceInfo(int numHapDev, int parameter)
	{
		// Haptic info variables
		// 0 - m_maxLinearForce
		// 1 - m_maxAngularTorque
		// 2 - m_maxGripperForce 
		// 3 - m_maxLinearStiffness
		// 4 - m_maxAngularStiffness
		// 5 - m_maxGripperLinearStiffness;
		// 6 - m_maxLinearDamping
		// 7 - m_maxAngularDamping
		// 8 - m_maxGripperAngularDamping

		float temp;
		switch (parameter)
		{
		case 1:
			temp = (float)HapticPluginImport.GetHapticsDeviceInfo(myHapticPlugin, numHapDev, 1);
			break;
		case 2:
			temp = (float)HapticPluginImport.GetHapticsDeviceInfo(myHapticPlugin, numHapDev, 2);
			break;
		case 3:
			temp = (float)HapticPluginImport.GetHapticsDeviceInfo(myHapticPlugin, numHapDev, 3);
			break;
		case 4:
			temp = (float)HapticPluginImport.GetHapticsDeviceInfo(myHapticPlugin, numHapDev, 4);
			break;
		case 5:
			temp = (float)HapticPluginImport.GetHapticsDeviceInfo(myHapticPlugin, numHapDev, 5);
			break;
		case 6:
			temp = (float)HapticPluginImport.GetHapticsDeviceInfo(myHapticPlugin, numHapDev, 6);
			break;
		case 7:
			temp = (float)HapticPluginImport.GetHapticsDeviceInfo(myHapticPlugin, numHapDev, 7);
			break;
		case 8:
			temp = (float)HapticPluginImport.GetHapticsDeviceInfo(myHapticPlugin, numHapDev, 8);
			break;
		default:
			temp = (float)HapticPluginImport.GetHapticsDeviceInfo(myHapticPlugin, numHapDev, 0);
			break;
		}
		return temp;
	}

	private void SetForceByDesiredPosition(int hapDevNum, Vector3 desiredPosition)
	{
		// compute linear force    
		Vector3 position = (hapDevNum == 0) ? positionLeft : positionRight;
		Vector3 direction = desiredPosition - position;
		Vector3 forceField  = HapticInteractionPoint.instance.Kp  * direction;

		// forceField = (forceField.magnitude > 20.0f) ? forceField.normalized * 20.0f : forceField;

		if (hapDevNum == 0) {
			Seesaw.instance.forceL = forceField.magnitude;
			Seesaw.instance.forceRedVector3= forceField;
		} else {
			Seesaw.instance.forceR = forceField.magnitude;
			Seesaw.instance.forceGreenVector3 = forceField;
		}
			
		HapticPluginImport.SetHapticsForce(myHapticPlugin, hapDevNum, forceField);
		// compute linear damping force
		Vector3 linearVelocity = HapticPluginImport.GetHapticsLinearVelocity(myHapticPlugin, hapDevNum);
		Vector3 forceDamping = HapticInteractionPoint.instance.Kv * linearVelocity * -1.0f;
		// sent force to haptic device
		HapticPluginImport.SetHapticsForce(myHapticPlugin, hapDevNum, forceDamping);

	}

}
