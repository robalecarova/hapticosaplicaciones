﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class Seesaw : MonoBehaviour {

	// Object Parameters
	public float mass;
	public float length;
	public float width;
	public float inercia;
	public float sumTorque;

	public float forceR;
	public float forceL;
	public float lengthR;
	public float lengthL;

	public float rotationAngle;

//	public ToggleGroup vectorsTG;

	// Singleton Instantiation
	public static Seesaw instance; 

	// Arrows
	public GameObject arrowL;
	public GameObject arrowR;

	public Text forcexyRed;
	public Text forcexyGreen;
	public Text forceRed;
	public Text forceGreen;
	public Text torqueRed;
	public Text torqueGreen;
	public Text lengthRed;
	public Text lengthGreen;
	public Text sumTorques;

	public Vector3 forceRedVector3;
	public Vector3 forceGreenVector3;
	public float torqueRedValue;
	public float torqueGreenValue;

	// Other values
	static float maxRangeTopArrow = 10.0f;
	static float maxForce = 20.0f;
	static float minForce = 0.0f;

	Vector3 objectRotation;

	Vector3 arrowLPosition, arrowLScale, arrowLRot;
	Vector3 arrowRPosition, arrowRScale,arrowRRot;

	void Awake(){
		instance = this;
	}


	// Use this for initialization
	void Start () {
		mass = 1.0f;
		length = this.transform.localScale.x;
		width = this.transform.localScale.z;
		inercia = (mass * ((length * length) + (width * width)) ) / 12.0f;
		rotationAngle = 0.0f;
		sumTorque = 0.0f;

		forceL = 0.0f;
		forceR = 0.0f;

		arrowLScale = new Vector3 (0.0f, 5.0f, 1.0f);
		arrowRScale = new Vector3 (0.0f, 5.0f, 1.0f);

		// arrowLPosition = new Vector3 (-25.0f, 0.0f, -50f);
		// arrowRPosition = new Vector3 (-115.0f, 0.0f, -50f);

		arrowLPosition = new Vector3 (0.0f, 0.0f, -50f);
		arrowRPosition = new Vector3 (0.0f, 0.0f, -50f);

		objectRotation = this.transform.localEulerAngles;

		forceRedVector3 = new Vector3 (0.0f, 0.0f, 0.0f);
		forceGreenVector3= new Vector3 (0.0f, 0.0f, 0.0f);
		torqueRedValue= 0.0f;
		torqueGreenValue= 0.0f;
		
	}



	// Update is called once per frame
	int counterUpdateValues = 0;
	void Update () {
		// Seesaw behaviour
		objectRotation.z = rotationAngle;
		this.transform.localEulerAngles = objectRotation;

		if(counterUpdateValues % 10 == 0)
		{
			forcexyRed.text = forceRedVector3.x.ToString ("F1") + "," + forceRedVector3.y.ToString ("F1");
			forcexyGreen.text = forceGreenVector3.x.ToString ("F1") + "," + forceGreenVector3.y.ToString ("F1");

			forceRed.text = forceRedVector3.magnitude.ToString ("F1");
			forceGreen.text = forceGreenVector3.magnitude.ToString ("F1");
			torqueRed.text = torqueRedValue.ToString ("F1");
			torqueGreen.text = torqueGreenValue.ToString ("F1");
			lengthRed.text = lengthL.ToString ("F1");
			lengthGreen.text = lengthR.ToString ("F1");
			sumTorques.text = sumTorque.ToString ("F3");

		// Limpiamos los valores despues de haberlos puesto en escena
			forceRedVector3 = new Vector3 (0.0f, 0.0f, 0.0f);
			forceGreenVector3= new Vector3 (0.0f, 0.0f, 0.0f);
			torqueRedValue= 0.0f;
			torqueGreenValue= 0.0f;
			sumTorque = 0.0f;

			arrowL.SetActive (true);
			arrowL.transform.localScale = arrowLScale;
			arrowL.transform.position = arrowLPosition;
			arrowL.transform.localEulerAngles = arrowLRot;

			arrowR.SetActive (true);
			arrowR.transform.localScale = arrowRScale;
			arrowR.transform.position = arrowRPosition;
			arrowR.transform.localEulerAngles = arrowRRot;
		}
		counterUpdateValues++;
	}
		
	void FixedUpdate(){

		sumTorque = 0.0f;

		Vector3 tempLength = HapticInteractionPoint.instance.transform.position;
		tempLength.z = 0.0f;
		tempLength.y = 0.0f;
		lengthL = tempLength.magnitude;

		tempLength = HapticInteractionPoint2.instance.transform.position;
		tempLength.z = 0.0f;
		tempLength.y = 0.0f;
		lengthR = tempLength.magnitude;

		torqueGreenValue = (forceR*lengthR);
		torqueGreenValue /= inercia;
		torqueRedValue = (forceL*lengthL);
		torqueRedValue /= inercia;

		sumTorque = torqueGreenValue - torqueRedValue;

		sumTorque = (Mathf.Abs(sumTorque) > 0.1f) ? sumTorque : 0.0f;

		rotationAngle +=  -sumTorque / (10.0f);


		if (rotationAngle > 30.0f) {
			rotationAngle = 30.0f;
		} else if (rotationAngle < -30.0f) {
			rotationAngle = -30.0f;
		}

		// Left force
		arrowLPosition = HapticInteractionPoint.instance.desiredPosition;
		arrowLPosition.z = -50;	


		arrowLScale.x = (forceL - minForce)/ (maxForce - minForce); // torqueize value (0,1)
		arrowLScale.x = (arrowLScale.x * maxRangeTopArrow); // Range (0, maxRangeTopArrow)
		arrowLRot = arrowL.transform.localEulerAngles;
		arrowLRot.z = 90 + rotationAngle;


		// Right force
		arrowRPosition = HapticInteractionPoint2.instance.desiredPosition;
		arrowRPosition.z = -50;	

		arrowRScale.x = (forceR - minForce)/ (maxForce - minForce); // torqueize value (0,1)
		arrowRScale.x = (arrowRScale.x * maxRangeTopArrow) ; // Range (0, maxRangeTopArrow)
		arrowRRot = arrowR.transform.localEulerAngles;
		arrowRRot.z = 90 + rotationAngle;
	
	}
}

