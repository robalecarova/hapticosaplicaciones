﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class MassSpring : MonoBehaviour {

	public enum Conf {Basic, Serial, Parallel};

	public Conf currentState;

	//Parameters
	public float k1, k2, k;
	public Text textK,  forceText, appliedForceText, textX;
	public InputField k01, k11, k21, k12, k22, massUI;
	public Slider ks01, ks11, ks21, ks12, ks22, massSlider;
	public float force, forceK;
	public float massZ;
	public float shrinkPosition;

	// Singleton Instantiation
	public static MassSpring instance; 

	// Arrows

	public GameObject mass;
	public GameObject spring;

    float tempDistance;

    readonly float maxDeltaZ = 80.0f;
    readonly float initialMassPositionZ = -60.0f;
    readonly float maxX = -70.0f;
    readonly float minX = 20.0f;
    readonly float epsilonDistance = 1.0f;
    readonly float maxForce = 10;
    readonly float minForce = 0;


    Vector3 scaleSpring;
	Vector3 massPosition;

	public GameObject arrowSpring;
	Vector3 arrowSpringPosition;
	Vector3 arrowSpringScale;


	void Awake(){
		instance = this;
	}


	// Use this for initialization
	void Start () {

		currentState = Conf.Basic;

		k1 = 10.0f;
		k2 = 10.0f;
		force = 0.0f;
		forceK = 0.0f;
		shrinkPosition = 1.0f;

		scaleSpring = new Vector3 (1.0f, 1.0f, 1.0f);
		massPosition = new Vector3 (0.0f, 25.0f, -60.0f);

		k01.text = k1.ToString();
		k11.text = k1.ToString();
		k21.text = k1.ToString();

		k12.text = k2.ToString();
		k22.text = k2.ToString();

		arrowSpringScale = new Vector3 (20, 100, 1);
		arrowSpringPosition = new Vector3 (30, 40, 1);
	}


	int counterUpdateGraphics = 0;
	// Update is called once per frame
	void Update () {
		scaleSpring.z = (float.IsNaN(scaleSpring.z)) ? 1.0f : scaleSpring.z;
		massPosition.z = (float.IsNaN(massPosition.z)) ? -60.0f : massPosition.z;

        Vector3 desplazar = massPosition - mass.transform.localPosition;
        //HapticNativePlugin.TranslateObject(0, desplazar);
        mass.transform.Translate(desplazar); // * Time.deltaTime);
        HapticNativePlugin.TranslateObject(0, desplazar);



        if (scaleSpring.z != 1.0) {
				spring.transform.localScale = scaleSpring;
		}

		switch (currentState) {
			case Conf.Parallel:
				k = k1 + k2;
				break;
			case Conf.Serial:
				k = (k1*k2) / (k1 + k2);
				break;
			default:
				k = k1;
				break;
		}

		// Actualizar etiquetas

		textK.text = k.ToString();

		if(counterUpdateGraphics % 10 == 0)
		{
			textX.text = (initialMassPositionZ - massPosition.z).ToString("F1");
			forceText.text = forceK.ToString("F1");
			appliedForceText.text = force.ToString("F1");
			force = 0.0f;
			
			// compute arrows
			arrowSpringScale.x = (forceK)/ (maxForce - minForce); // normalize value (0,1)
			arrowSpringScale.x = (arrowSpringScale.x * 40); // Range (0.01, maxRangeTopArrow)
			arrowSpring.transform.localScale = arrowSpringScale;
			
			// it is divided by two, to get the object's center
			arrowSpringPosition.x = mass.transform.position.x;
			arrowSpring.transform.position = arrowSpringPosition;
		}
		counterUpdateGraphics++;	
	}
	
	void FixedUpdate(){
		// Nueva posicion masa y resorte
		massPosition = mass.transform.localPosition;
		forceK = (initialMassPositionZ - massPosition.z) * (k/100); // Dividido entre 100 porque son cm

		tempDistance =  ((force + forceK) / k);
        massZ = massPosition.z + tempDistance;

        massZ = (massZ > minX) ? minX: massZ;
		massZ = (massZ < maxX) ? maxX: massZ; // Para cuando jala el resorte, en lugar de comprimirlo
		massPosition.z = massZ;
//
		shrinkPosition = (massZ - maxDeltaZ) / (initialMassPositionZ - maxDeltaZ); //Normalization step
		shrinkPosition = (shrinkPosition < 0.2f) ? 0.2f : shrinkPosition;
		shrinkPosition = (shrinkPosition > 1.2f) ? 1.2f : shrinkPosition;
		scaleSpring.z = shrinkPosition;
	}
}

