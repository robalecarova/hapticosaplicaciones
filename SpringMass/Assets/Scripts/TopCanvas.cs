﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TopCanvas : MonoBehaviour {

	public Canvas configuracion, instructions;
	public GameObject basic, serial, parallel;

	void Start(){
		instructions.enabled = false;
		MassSpring.instance.massUI.text = "1";
	}
	public void SelectInst(){
		instructions.enabled = true;
	}

	public void DisableInst(){
		instructions.enabled = false;
	}

	public void SelectConf(){
		configuracion.enabled = true;
	}

	public void DisableConf(){
		configuracion.enabled = false;
	}

	public void BasicConf(){
		MassSpring.instance.currentState = MassSpring.Conf.Basic;
		basic.SetActive (true);
		serial.SetActive (false);
		parallel.SetActive (false);
		configuracion.enabled = false;
	}

	public void SerialConf(){
		MassSpring.instance.currentState = MassSpring.Conf.Serial;
		basic.SetActive (false);
		serial.SetActive (false);
		parallel.SetActive (true);
		configuracion.enabled = false;
	}

	public void ParallelConf(){
		MassSpring.instance.currentState = MassSpring.Conf.Parallel;
		basic.SetActive (false);
		serial.SetActive (true);
		parallel.SetActive (false);
		configuracion.enabled = false;
	}

	public void setTextK1(float newValue)
	{
		MassSpring.instance.k01.text = newValue.ToString();
		MassSpring.instance.k11.text = newValue.ToString();
		MassSpring.instance.k21.text = newValue.ToString();

		MassSpring.instance.k1 = newValue;
	}

		public void setSliderK1(string newText)
	{
		float temp = float.Parse (newText);

		MassSpring.instance.ks01.value = temp;
		MassSpring.instance.ks11.value = temp;
		MassSpring.instance.ks21.value = temp;
	}

		public void setTextK2(float newValue)
	{
		MassSpring.instance.k12.text = newValue.ToString();
		MassSpring.instance.k22.text = newValue.ToString();

		MassSpring.instance.k2 = newValue;
	}

		public void setSliderK2(string newText)
	{
		float temp = float.Parse (newText);
		MassSpring.instance.ks12.value = temp;
		MassSpring.instance.ks22.value = temp;
		
	}

		public void setTextMass(float newValue)
	{
		MassSpring.instance.massUI.text = newValue.ToString();
	}

		public void setSliderMass(string newText)
	{
		float temp = float.Parse (newText);
		MassSpring.instance.massSlider.value = temp;		
	}


}
