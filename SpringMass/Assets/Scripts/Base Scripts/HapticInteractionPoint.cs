﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HapticInteractionPoint : MonoBehaviour {
    
    // establish Haptic Manager and IHIP objects
    public GameObject hapticManager;
    public GameObject IHIP;

    // get haptic device information from the haptic manager
    private HapticManager myHapticManager;
    
    // haptic device number
    public int hapticDevice;
    // haptic device variables
    private Vector3 position;
    private bool button0;
    public float mass;
    private Rigidbody rigidBody;

	// haptic device variables
	public float radius;
	public Vector3 desiredPosition;

	// hip colliding flag
	public bool isColliding;

	// object in the scene that was hitted
	private float objectMass;
	private string objectColliding;
	private Vector3 HIPCollidingPosition;


	[Header("Attraction Force")]
	// stiffness coefficient
	public float Kp; // [N/m]

	[Header("Damping Factors")]
	// damping term
	public float Kv; // [N/m]

	// Singleton instance
	public static HapticInteractionPoint instance; 

	// MaxValues
	float maxKv;
	float maxKvr;
	float maxKvg;

    // Called when the script instance is being loaded
    void Awake() {
        position = new Vector3(0, 0, 0);
        button0 = false;
        rigidBody = GetComponent<Rigidbody>();
		isColliding = false;

		instance = this;
    }

    // Use this for initialization
    void Start () {
        myHapticManager = (HapticManager)hapticManager.GetComponent(typeof(HapticManager));
		radius = IHIP.GetComponent<Renderer> ().bounds.extents.magnitude / 2.0f;

		// set haptic device mass
		mass = (mass > 0) ? mass : 0.0f;
		rigidBody.mass = mass;

		Kp = 5;
		Kv = 20; // [N/m]
	}
	
	// Update is called once per frame
	void Update () {

        // get haptic device to be used
        int hapticsFound = myHapticManager.GetHapticDevicesFound();
        hapticDevice = (hapticDevice > -1 && hapticDevice < hapticsFound) ? hapticDevice : hapticsFound - 1;

        // get haptic device variables
        position = myHapticManager.GetPosition(hapticDevice);
        button0 = myHapticManager.GetButtonState(hapticDevice, 0);

        // update positions of HIP and IHIP
		if (isColliding) {
			if (button0)
			{
				HIPCollidingPosition.x = 0.0f;
				position.x = 0.0f;
			}
			IHIP.transform.position = HIPCollidingPosition;
//			IHIP.transform.position = position;
			this.transform.position = position;
		} else {
			IHIP.transform.position = position;
			this.transform.position = position;
		}
				
		// update damping factors
		maxKv = myHapticManager.GetHapticDeviceInfo(hapticDevice, 6);
		Kv = (Kv > maxKv) ? maxKv : Kv;
    }

	 void OnCollisionStay(Collision collision)
	{
		if (collision.gameObject.name == "Mass") {
			isColliding = true;
			if (Mathf.Abs (collision.contacts [0].separation) > radius) {
				HIPCollidingPosition = collision.contacts [0].point + (Mathf.Abs (collision.contacts [0].separation) * collision.contacts [0].normal);
			} else {
				HIPCollidingPosition = collision.contacts [0].point + (radius * collision.contacts [0].normal);
			}
			// update collision point
			desiredPosition = position + (collision.contacts [0].normal * Mathf.Abs (collision.contacts [0].separation));
		}
	}
		

	void OnCollisionExit(Collision collision)
	{
		if (collision.gameObject.name == "Mass") {
			isColliding = false;
		}
	}
		
}

